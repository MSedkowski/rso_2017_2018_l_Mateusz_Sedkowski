#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <time.h>
#include <memory.h>
#include "client.h"

int is_big_endian() {
    int test = 1;
    char *p = (char*)&test;

    return p[0] == 0;
}

union byte8 {
    char byte[8];
    double value;
};

double endianSwap(double a) {

    union byte8 un;
    un.value = a;


    if(!is_big_endian()) {
        // swap
        char c1 = un.byte[0];
        un.byte[0] = un.byte[7];
        un.byte[7] = c1;
        c1 = un.byte[1];
        un.byte[1] = un.byte[6];
        un.byte[6] = c1;
        c1 = un.byte[2];
        un.byte[2] = un.byte[5];
        un.byte[5] = c1;
        c1 = un.byte[3];
        un.byte[3] = un.byte[4];
        un.byte[4] = c1;
    }

    return un.value;
}

int main ()
{
    int sockfd;
    socklen_t len;
    struct sockaddr_in address;
    int result;
    SqrtStruct sqrtStruct;
    TimeStruct timeStruct;
    HeaderStruct headerStruct;
    int select;
    double value;
    int request_id = 1;

    /* Tworzenie Socket'a  */

    sockfd = socket (AF_INET, SOCK_STREAM, 0);

    /*  Konfiguracja  */

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = inet_addr ("127.0.0.1");
    address.sin_port = htons (9734);
    len = sizeof (address);

    /* Podłączenie  */
    result = connect (sockfd, (struct sockaddr *) &address, len);

    if (result == -1)
    {
        perror ("oops: netclient");
        exit (1);
    }
    /*  Wysyłanie/odbiór danych  */\
        printf("Wybierz opcję: \n");
        printf("1 - Obliczenie pierwiastka,\t 2 - Zapytanie o czas serwera\t 3 - Koniec\n");
        printf("Twój wybór: ");
        scanf("%d", &select);
        switch (select) {
            case 1: {
                printf("Podaj liczbę: ");
                scanf("%lf", &value);
                if(value < 0)
                {
                    printf("Podana liczba jest nieprawidłowa!\n");
                    headerStruct.bit3 = 4;
                    write(sockfd, &headerStruct, sizeof(headerStruct));
                }
                else {
                    value = endianSwap(value);
                    sqrtStruct.value = value;
                    sqrtStruct.bit0 = headerStruct.bit0 = 0;
                    sqrtStruct.bit1 = headerStruct.bit1 = 0;
                    sqrtStruct.bit2 = headerStruct.bit2 = 0;
                    sqrtStruct.bit3 = headerStruct.bit3 = 1;
                    sqrtStruct.request_id = request_id;
                    write(sockfd, &headerStruct, sizeof(headerStruct));
                    write(sockfd, &sqrtStruct, sizeof(sqrtStruct));
                    read(sockfd, &sqrtStruct, sizeof(sqrtStruct));
                    printf("Pierwiastek liczby %.2f = %.2f\n", endianSwap(value), endianSwap(sqrtStruct.value));
                }
                break;
            }
            case 2: {
                timeStruct.bit0 = headerStruct.bit0 = 0;
                timeStruct.bit1 = headerStruct.bit1 = 0;
                timeStruct.bit2 = headerStruct.bit2 = 0;
                timeStruct.bit3 = headerStruct.bit3 = 2;
                timeStruct.request_id = request_id;
                write(sockfd, &headerStruct, sizeof(headerStruct));
                write(sockfd, &timeStruct, sizeof(timeStruct));
                read(sockfd, &timeStruct, sizeof(timeStruct));
                struct tm *timeinfo = localtime(&timeStruct.time);
                printf("Czas serwera: %s\t", asctime(timeinfo));
                break;
            }
            case 3: {
                headerStruct.bit3 = 3;
                write(sockfd, &headerStruct, sizeof(headerStruct));
                printf("Dziękuję za użycie programu.\t");
                break;
            }
            default: {
                printf("Podano złą wartość!");
            }
        }
    close(sockfd);
    exit(0);
}