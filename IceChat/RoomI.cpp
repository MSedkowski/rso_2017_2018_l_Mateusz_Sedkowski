#include "RoomI.h"

namespace ChatApp {
    string RoomI::getName(const ::Ice::Current&) {
        return roomName;
    }

    UserList RoomI::getUsers(const ::Ice::Current&) {
        return users;
    }

    void RoomI::AddUser(const UserPrx& user, const ::Ice::Current&) {
        if (find(userNames.begin(), userNames.end(), user->getName()) != userNames.end()) {
            throw UserAlreadyExists();
        }
        users.push_back(user);
        userNames.push_back(user->getName());
    }

    void RoomI::LeaveRoom(const UserPrx& user, const ::Ice::Current&) {
        auto userSavedInRoom = find(userNames.begin(), userNames.end(), user->getName());
        if (userSavedInRoom == userNames.end()) {
            throw NoSuchUserExist();
        }
        userNames.erase(userSavedInRoom);
        for (auto usersIterator = users.begin(); usersIterator != users.end(); ++usersIterator) {
            if ((*usersIterator) == user) {
                usersIterator = users.erase(usersIterator);
                break;
            } 
        }
        cout << "Removed user " << user->getName() << " from room" << endl;
    }

    void RoomI::Destroy(const ::Ice::Current&) {
        userNames.clear();
        users.clear();
    }

    void RoomI::SendMessage(const UserPrx& user, const string& message, const ::Ice::Current&) {
        auto userSavedInRoom = find(userNames.begin(), userNames.end(), user->getName());
        if (userSavedInRoom == userNames.end()) {
            throw NoSuchUserExist();
        }

        for (auto& userInRoom : users) {
            userInRoom->SendMessage(roomName, user, message);
        }
    }
}