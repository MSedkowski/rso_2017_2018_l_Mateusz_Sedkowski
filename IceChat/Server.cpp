#include <Ice/Ice.h>
#include "chat.h"
#include "ServerI.h"

using namespace ChatApp;
using namespace std;

int main(int argc, char* argv[]) {
    int status = 0;
    Ice::CommunicatorPtr iceCommunicator;
    try {
        iceCommunicator = Ice::initialize(argc, argv);
        Ice::ObjectAdapterPtr adapter = iceCommunicator->createObjectAdapterWithEndpoints("ServerAdapter", "default -p 10000");
        Ice::ObjectPtr object = new ServerI();
        adapter->add(object, iceCommunicator->stringToIdentity("Server"));
        adapter->activate();
        iceCommunicator->waitForShutdown();
    } catch (const char* msg) {
        cerr << msg << endl;
        status = 1;
    } catch (const RoomAlreadyExist &ex) {
        cerr << "Room already exist" << endl;
    } catch (const NoSuchRoomExist& ex) {
            cerr << "Room dosen't exist" << endl;
    } catch (const UserAlreadyExists& ex) {
            cerr << "Such user already exist" << endl;
    } catch (const Ice::Exception& e) {
        cerr << e << endl;
        status = 1;
    } 
    if (iceCommunicator) {
        try {
            iceCommunicator->destroy();
        } catch (const Ice::Exception& e) {
            cerr << e << endl;
            status = 1;
        }
    }
    return status;
}
