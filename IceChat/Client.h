#ifndef CLIENT_H
    #define CLIENT_H

    #include <thread>
    #include <Ice/Ice.h>
    #include "chat.h"
    #include "UserI.h"
    
    using namespace std;
    using namespace Chat;
    using namespace ChatApp;

    namespace ClientApp {
        class Client {
            public:
                Client(const string&);
                void createRoom() const;
                void printListAllRooms() const;
                void joinToRoom();
                void printUsersInRoom() const;
                void leaveRoom();
                void sendPrivateMessageToUser() const;
                void sendMessageToRoom() const;
                void printAllUsers() const;
                ~Client();
            private:
                string username;
                UserPrx user;
                ServerPrx server;
                Ice::CommunicatorPtr iceCommunicator;
                Ice::ObjectAdapterPtr adapter;
                RoomList userRooms;

                void createUser();
                string getNameOfTheRoom() const;
                void scrollConsole() const;
                UserList getUsersInRoom() const;
        }; 
    }

#endif