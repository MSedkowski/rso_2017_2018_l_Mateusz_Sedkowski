﻿using System;
using System.Runtime.Serialization;

namespace Server
{
    [DataContract]
    public class BorrowData
    {
        [DataMember]
        private User user;

        [DataMember]
        private DateTime returnDate;

        public BorrowData(User user, DateTime returnDate)
        {
            this.User = user;
            this.returnDate = returnDate;
        }

        public DateTime ReturnDate
        {
            get
            {
                return returnDate;
            }

            set
            {
                returnDate = value;
            }
        }

        public User User
        {
            get
            {
                return user;
            }

            set
            {
                user = value;
            }
        }




    }


}
