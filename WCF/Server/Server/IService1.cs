﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Server
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {
        [OperationContract]
        List<Book> GetRentedBooks();

        [OperationContract]
        BookList getAllBooks();

        [OperationContract]
        [FaultContract(typeof(UserNotFound))]
        List<Book> getBorrowedBooks(int userID);

        [OperationContract]
        [FaultContract(typeof(BookNotFoundException))]
        Book getBookFromList(int bookID);

        [OperationContract]
        [FaultContract(typeof(BookNotFoundException))]
        Boolean checkIfBookIsBorrowed(int bookID);
    }

}
