﻿using System;
using System.Runtime.Serialization;

namespace Server
{
    [DataContract]
    public class Book
    {
        [DataMember]
        private int bookId;

        [DataMember]
        private String name;

        [DataMember]
        private String author;

        [DataMember]
        private BookType type;

        [DataMember]
        private BorrowData borrowData;
        public Book(int bookID, string name, string author, BookType type)
        {
            this.bookId = bookID;
            this.Name = name;
            this.Author = author;
            this.Type = type;
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public string Author
        {
            get
            {
                return author;
            }

            set
            {
                author = value;
            }
        }

        internal BookType Type
        {
            get
            {
                return type;
            }

            set
            {
                type = value;
            }
        }

        public BorrowData BorrowData
        {
            get
            {
                return borrowData;
            }

            set
            {
                borrowData = value;
            }
        }

        public int BookId
        {
            get
            {
                return bookId;
            }

            set
            {
                bookId = value;
            }
        }
    }

    public enum BookType
    {
        COMEDY, DRAMA, FANTASY, HORROR, THRILLER
    }


}