﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Server
{
    [DataContract]
    public class BookList
    {
        [DataMember]
        private List<Book> listOfBooks = new List<Book>();

        [DataMember]
        private List<User> listOfUsers = new List<User>();

        public BookList()
        {
            User vincent = new User(1, "Vincent");
            User adam = new User(2, "Adam");
            Book book1 = new Book(1, "To", "Stephen King", BookType.HORROR);
            Book book2 = new Book(2, "Eragon", "Christopher Paolini", BookType.FANTASY);
            Book book3 = new Book(3, "Harry Potter i kamień filozoficzny", "J.K.Rowling", BookType.FANTASY);
            Book book4 = new Book(4, "Ania z Zielonego wzgórza", "L. M. Montgomery", BookType.COMEDY);
            book1.BorrowData = new BorrowData(vincent, new DateTime(2018, 5, 31));
            book2.BorrowData = new BorrowData(vincent, new DateTime(2018, 5, 31));
            book4.BorrowData = new BorrowData(adam, new DateTime(2018, 5, 31));
            listOfBooks.Add(book1);
            listOfBooks.Add(book2);
            listOfBooks.Add(book3);
            listOfBooks.Add(book4);
            listOfUsers.Add(vincent);
            listOfUsers.Add(adam);
        }

        internal List<Book> getBorrowedBooks(int userID)
        {
            if (!listOfUsers.Exists(x => x.UserID == userID))
            {
                return null;
            }
            List<Book> usersBorrowedBooks = new List<Book>();
            foreach (Book book in listOfBooks)
            {
                if (book.BorrowData != null && book.BorrowData.User.UserID == userID)
                {
                    usersBorrowedBooks.Add(book);
                }
            }
            return usersBorrowedBooks;
        }

        internal Book getBookInfo(int bookID)
        {
            foreach (Book book in listOfBooks)
            {
                if (book.BookId.Equals(bookID))
                {
                    return book;
                }
            }
            return null;
        }

        public List<Book> ListOfBooks
        {
            get
            {
                return listOfBooks;
            }

            set
            {
                listOfBooks = value;
            }
        }

        public Book getBookByName(String name)
        {
            foreach (Book book in ListOfBooks)
            {
                if (book.Name.Equals(name))
                {
                    return book;
                }
            }
            return null;
        }

        public List<Book> getRentedBooks()
        {
            List<Book> listOfRentedBooks = new List<Book>();
            foreach (Book book in listOfBooks)
            {
                if (book.BorrowData != null)
                {
                    listOfRentedBooks.Add(book);
                }
            }
            return listOfRentedBooks;
        }


    }


}
