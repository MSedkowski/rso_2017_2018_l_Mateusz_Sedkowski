﻿using System.Runtime.Serialization;

namespace Server
{
    [DataContract]
    public class User
    {
        [DataMember]
        private int userID;
        [DataMember]
        private string userName;

        public User(int userID, string userName)
        {
            this.userID = userID;
            this.userName = userName;
        }

        public int UserID
        {
            get
            {
                return userID;
            }

            set
            {
                userID = value;
            }
        }

        public string UserName
        {
            get
            {
                return userName;
            }

            set
            {
                userName = value;
            }
        }
    }
}