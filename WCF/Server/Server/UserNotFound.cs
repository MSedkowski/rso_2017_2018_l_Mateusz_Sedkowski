﻿using System;
using System.Runtime.Serialization;
namespace Server
{
    [DataContract]
    [Serializable]
    internal class UserNotFound
    {
        private string _message;

        public UserNotFound(string message)
        {
            _message = message;
        }

        [DataMember]
        public string Message { get { return _message; } set { _message = value; } }
    }
}