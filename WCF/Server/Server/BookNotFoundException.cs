﻿using System;
using System.Runtime.Serialization;

namespace Server
{
    [DataContract]
    [Serializable]
    internal class BookNotFoundException
    {
        private string _message;

        public BookNotFoundException(string message)
        {
            _message = message;
        }

        [DataMember]
        public string Message { get { return _message; } set { _message = value; } }
    }
}