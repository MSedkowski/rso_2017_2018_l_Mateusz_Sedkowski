﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Server
{
    public class Service1 : IService1
    {
        private static BookList listOfBooks = new BookList();

        public BookList getAllBooks()
        {
            return listOfBooks;
        }

        public List<Book> getBorrowedBooks(int userID)
        {
            List<Book> returnedList = listOfBooks.getBorrowedBooks(userID);
            if (returnedList == null)
            {
                throw new FaultException<UserNotFound>(new UserNotFound("Brak zarejestrowanego użytkownika!"));
                return null;
            }
            else
            {
                return returnedList;
            }
        }

        public List<Book> GetRentedBooks()
        {
            return listOfBooks.getRentedBooks();
        }

        public Book getBookFromList(int bookID)
        {
            Book returnedBook = listOfBooks.getBookInfo(bookID);
            if (returnedBook == null)
            {
                throw new FaultException<BookNotFoundException>(new BookNotFoundException("Książki nie znaleziono w zbiorze!"));
                return null;
            }
            else
            {
                return returnedBook;
            }
        }

        public Boolean checkIfBookIsBorrowed(int bookID)
        {
            Book returnedBook = listOfBooks.getBookInfo(bookID);
            if (returnedBook == null)
            {
                throw new FaultException<BookNotFoundException>(new BookNotFoundException("Książki nie znaleziono w zbiorze!"));
            }
            else
            {
                if (returnedBook.BorrowData == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

    }
}
