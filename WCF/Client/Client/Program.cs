﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Client.ServiceReference1;
using System.ServiceModel;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            Service1Client server = new Service1Client();

            Console.WriteLine("Aktualnie wypożyczone pozycje:");
            foreach (Book book in server.GetRentedBooks())
            {
                Console.WriteLine("Tytuł: " + book.name + " Autor: " + book.author + " UserID: " + book.borrowData.user.userID + " Data zwrotu: " + book.borrowData.returnDate);
            }
            Console.WriteLine("----------------------------------------------------");

            Console.WriteLine("Lista książek:");
            foreach (Book book in server.getAllBooks().listOfBooks)
            {
                Console.WriteLine("Tytuł: " + book.name + " Autor: " + book.author + " Typ: " + book.type);
            }
            Console.WriteLine("----------------------------------------------------");

            Console.WriteLine("Wyszukaj konkretnej książki:");
            int bookid1 = 2, bookid2 = 6;
            try
            {
                Book book1 = server.getBookFromList(bookid1);
                Console.WriteLine("Tytuł: " + book1.name + " Autor: " + book1.author + " Typ: " + book1.type);
                Book book2 = server.getBookFromList(bookid2);
            }
            catch (FaultException<BookNotFoundException> e)
            {
                Console.WriteLine("Id: " + bookid2 + " " + e.Detail.Message);
            }
            Console.WriteLine("----------------------------------------------------");

            Console.WriteLine("Wyszukaj wypożyczone książki konkretnego użytkownika:");
            int userid1 = 1, userid2 = 4;
            try
            {
                foreach (Book book in server.getBorrowedBooks(userid1))
                {
                    Console.WriteLine("Tytuł: " + book.name + " Data zwrotu: " + book.borrowData.returnDate);
                }
                foreach (Book book in server.getBorrowedBooks(userid2))
                {
                    Console.WriteLine("Tytuł: " + book.name + " Data zwrotu: " + book.borrowData.returnDate);
                }
            }
            catch (FaultException<UserNotFound> e)
            {
                Console.WriteLine(e.Detail.Message);
            }
            Console.WriteLine("----------------------------------------------------");

            Console.WriteLine("Sprawdź czy książka o podanym id jest dostępna:");
            try
            {
                if (server.checkIfBookIsBorrowed(bookid1))
                {
                    Console.WriteLine("Książka o id: " + bookid1 + " jest niedostępna");
                }
                else
                {
                    Console.WriteLine("Książka o id: " + bookid1 + " jest dostępna");
                }

                if (server.checkIfBookIsBorrowed(bookid2))
                {
                    Console.WriteLine("Książka o id: " + bookid2 + " jest niedostępna");
                }
                else
                {
                    Console.WriteLine("Książka o id: " + bookid2 + " jest dostępna");
                }
            }
            catch (FaultException<BookNotFoundException> e)
            {
                Console.WriteLine("Id: " + bookid2 + " " + e.Detail.Message);
            }
            Console.WriteLine("----------------------------------------------------");
            Console.ReadLine();
        }
    }
}
