//
// Created by vincent on 08.04.18.
//

#ifndef SERVER_SERVER_H
#define SERVER_SERVER_H

typedef struct sqrtRequest {
    char bit0;
    char bit1;
    char bit2;
    char bit3;
    int request_id;
    double value;
}SqrtStruct;

typedef struct timeRequest {
    char bit0;
    char bit1;
    char bit2;
    char bit3;
    int request_id;
    time_t time;
}TimeStruct;

typedef struct header {
    char bit0;
    char bit1;
    char bit2;
    char bit3;
}HeaderStruct;

#endif //SERVER_SERVER_H
