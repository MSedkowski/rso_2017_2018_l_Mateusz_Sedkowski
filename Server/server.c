#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <signal.h>
#include <unistd.h>
#include <math.h>
#include <sys/time.h>
#include <time.h>
#include "server.h"

union byte8 {
    char byte[8];
    double value;
};

double endianSwap(double a) {

    union byte8 un;
    un.value = a;

    // swap
    char c1 = un.byte[0];
    un.byte[0] = un.byte[7];
    un.byte[7] = c1;
    c1 = un.byte[1];
    un.byte[1] = un.byte[6];
    un.byte[6] = c1;
    c1 = un.byte[2];
    un.byte[2] = un.byte[5];
    un.byte[5] = c1;
    c1 = un.byte[3];
    un.byte[3] = un.byte[4];
    un.byte[4] = c1;

    return un.value;
}

int main () {
    int server_sockfd, client_sockfd;
    socklen_t server_len, client_len;
    struct sockaddr_in server_address;
    struct sockaddr_in client_address;
    SqrtStruct sqrtStruct;
    TimeStruct timeStruct;
    HeaderStruct headerStruct;

    server_sockfd = socket(AF_INET, SOCK_STREAM, 0);

    server_address.sin_family = AF_INET;
    server_address.sin_addr.s_addr = htonl(INADDR_ANY);
    server_address.sin_port = htons(9734);
    server_len = sizeof(server_address);
    bind(server_sockfd, (struct sockaddr *) &server_address, server_len);

    /*  Create a connection queue, ignore child exit details and wait for clients.  */

    listen(server_sockfd, 5);

    signal(SIGCHLD, SIG_IGN);

    while (1) {
        printf("server waiting\n");

        /*  Accept connection.  */

        client_len = sizeof(client_address);
        client_sockfd = accept(server_sockfd,
                               (struct sockaddr *) &client_address,
                               &client_len);

        /*  Fork to create a process for this client and perform a test to see
            whether we're the parent or the child.  */

        if (fork() == 0) {

            /*  If we're the child, we can now read/write to the client on client_sockfd.
                The five second delay is just for this demonstration.  */

            read(client_sockfd, &headerStruct, sizeof(headerStruct));
            switch (headerStruct.bit3) {
                case 1: {
                    read(client_sockfd, &sqrtStruct, sizeof(sqrtStruct));
                    sqrtStruct.value = endianSwap(sqrtStruct.value);
                    printf("%.2f\n", sqrtStruct.value);
                    sqrtStruct.value = sqrt(sqrtStruct.value);
                    sqrtStruct.value = endianSwap(sqrtStruct.value);
                    sqrtStruct.bit0 = 1;
                    write(client_sockfd, &sqrtStruct, sizeof(sqrtStruct));
                    write(client_sockfd, &headerStruct, sizeof(headerStruct));
                    break;
                }
                case 2: {
                    read(client_sockfd, &timeStruct, sizeof(timeStruct));
                    timeStruct.bit0 = 1;
                    timeStruct.time = time(NULL);
                    write(client_sockfd, &timeStruct, sizeof(timeStruct));
                    write(client_sockfd, &headerStruct, sizeof(headerStruct));
                    break;
                }
                case 3: {
                    headerStruct.bit3 = 3;
                    write(client_sockfd, &headerStruct, sizeof(headerStruct));
                    close(client_sockfd);
                    exit(0);
                }
                default: {
                }
            }
        }

            /*  Otherwise, we must be the parent and our work for this client is finished.  */

        else {
            close(client_sockfd);
        }
    }
}